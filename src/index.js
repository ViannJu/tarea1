const express = require("express");
const cors = require("cors");
const dbmongo = require("./base");

const port = process.env.PORT || 3000;

const app = express();
app.use(express());
app.use(cors());
app.get('/', function (req, res) {
    res.send('Bienvenido, ve a la ruta /carnet');
  });
app.get('/carnet', function (req, res) {
    res.send({nombre:'Viany Paola Juárez Hernández', carnet:'201700659'});
  });

app.listen(port, () => console.log(`Listening on port ${port}`));