const mongoose = require('mongoose')

mongoose.connect('mongodb://mongo/mibase')
    .then(db => console.log("Database is connected to the host =>", db.connection.host))
    .catch(err => console.error(err));

module.exports = mongoose